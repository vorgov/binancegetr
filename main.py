from influxdb import InfluxDBClient
import configparser

config = configparser.ConfigParser()
config.read('conf.ini')
influxConf = config['Influx']
db = InfluxDBClient(host=influxConf['host'], port=influxConf['port'], username=influxConf['user'], password=influxConf['password'])
db.create_database(influxConf['database'])
db.switch_database(influxConf['database'])

from binance.websockets import BinanceSocketManager
from binance.client import Client

binanceConf = config['Binance']
client = Client(binanceConf['apikey'],
                binanceConf['apisecret'], {"verify": False, "timeout": 20})


def process_message(msg):
    if msg['e'] == 'error':
        print('die with')
        print(msg)
    else:
        json_body = [
            {
                "measurement": "trade",
                "tags": {
                    "pair": binanceConf['pair']
                },
                "fields": msg
            }
        ]
        print(json_body)
        db.write_points(json_body)


bm = BinanceSocketManager(client)
conn_key = bm.start_trade_socket(binanceConf['pair'], process_message)
bm.start()


